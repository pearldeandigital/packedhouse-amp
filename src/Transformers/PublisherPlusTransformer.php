<?php

namespace Square1\Amp\Transformers;

use Square1\Amp\Transformers\Traits\RegexBuilder;
use Square1\Amp\Transformers\Traits\AmpScripts;
use Square1\Amp\Transformers\AbstractTransformer;
use Illuminate\Http\Request;

/**
* Parse post content into a compatible
* amp html
*/
class PublisherPlusTransformer extends AbstractTransformer
{
    use RegexBuilder;
    use AmpScripts;

    /**
     * Return the given content as it is
     *
     * @return mixed Post content (body)
     */
    public function getRawContent($html = '')
    {
        if (!empty($html)) {
            return $html;
        }

        return $this->data['content']['formatted'];
    }

    /**
     * Loads the scripts in the $scripts var
     *
     * @param  string $script google-amp script name
     *
     * @return $this->scripts Updated scripts var
     */
    public function loadScripts($script)
    {
        if (!empty($script)) {
            $tag = $this->getScriptTag($script);
            // Avoid duping tags
            if (!in_array($tag, $this->scripts)) {
                $this->scripts[] = $tag;
            }
        }
        return $this;
    }

    /**
     * Proccess the post content and return
     * the formatted google-amp version
     *
     * @param  string $content Post content
     *
     * @return string $content Post content (amp version)
     */
    protected function process($content)
    {
        $url = $this->getPostUrl();

        /* Style && css formatting */
        $content = $this->removeStyleAttributes($content);
        $content = $this->removeLinkStyleTags($content);
        $content = $this->removeStyleTags($content);
        /* end */

        /* Image Slider formatting - it must be before formatting images */
        $content = $this->parseImageSlider($content);

        /* Image formatting */
        $content = $this->parseImages($content);
        /* end */

        /* Third party content formatting */
        $content = $this->parseTwitterQuotes($content);
        $content = $this->parseInstagramQuotes($content);
        $content = $this->parseVineEmbeds($content);
        $content = $this->parseImgUrQuotes($content, $url);
        $content = $this->parseVimeoEmbeds($content);
        $content = $this->parseOoyalaEmbeds($content, $url);
        $content = $this->parseWufooEmbeds($content, $url);
        $content = $this->parseVideoEmbeds($content);
        $content = $this->parseMultiSourceVideo($content);
        $content = $this->parseAudioTags($content);

        $content = $this->parseAmpRiddleQuiz($content);

        // Some facebook posts are like a regular iframe. That's why this one
        // needs to be called before the parseIframes function
        $content = $this->parseFacebookEmbeds($content);
        $content = $this->parsePlayBuzzEmbeds($content, $url);
        $content = $this->parsePooldaddyScripts($content, $url);
        $content = $this->parseJwplayerContent($content);

        /* Standard HTML Tags format */
        $content = $this->parseIframes($content);
        $content = $this->cleanLinks($content);
        $content = $this->parseHtmlForms($content, $url);
        $content = $this->cleanEmptyLines($content);
        $content = $this->removeScriptTags($content);
        $content = $this->removeObjectTags($content, $url);
        $content = $this->removeInvalidAttributes($content);
        $content = $this->removeMapTags($content, $url);
        $content = $this->parseApesterTags($content);
        $content = $this->parseApesterDivs($content);
        $content = $this->parseGoogleMapsIframes($content);

        /* Parse embedly card quote */
        $content = $this->parseEmbedlyCardQuote($content, $url);

        /* Parse Brid Player */
        $content = $this->parseBridPlayer($content, $url);

        /* Parse Brightcove Player */
        $content = $this->parseBrightcovePlayer($content, $url);

        /* remove embed tags from tubechop */
        $content = $this->removeEmbedTagsFromTubechop($content, $url);

        /* Change value of target attribute from blank to _blank */
        $content = $this->changeValueTargetAttribute($content);

        /* Parse live blog of Arena.im */
        $content = $this->parseLiveBlogArenaIm($content);

        /** remove param tags
         *   This function should always be last,
         *  the param tags possibly be used in the functions above
        **/
        $content = $this->removeParamTags($content);
        $content = $this->removeUnsupportedEmbeds($content, $url);
        $content = $this->removeUnsupportedIframes($content, $url);
        $content = $this->removeUnsupportedImg($content, $url);
        $content = $this->removeUnsupportedInteraction($content, $url);
        $content = $this->removeUnsupportedObject($content, $url);
        $content = $this->removeUnsupportedJsEvents($content);

        return $content;
    }

    /**
     * Support <audio> tags
     *
     * @param string  $content
     *
     * @return string Formatted content
     */
    public function parseAudioTags($content)
    {
        $pattern = '<audio[^>]*?src="(.*?)"[^>]*?>(.*?)<\/audio>';

        $replacement = '<amp-audio width="400" height="60" src="$1">
                            <div fallback>$2</div>
                        </amp-audio>';

        $this->loadScripts('amp-audio');

        return $this->replace($pattern, $replacement, $content);
    }

    /**
     * Support <video> tags
     *
     * @param string  $content
     *
     * @return string Formatted content
     */
    public function parseVideoEmbeds($content)
    {
        $pattern = '<video[^>]*?src="(.*?)"[^>]*?>(<\/video>)?';
        $replacement = '<amp-video controls
                            width="640"
                            height="360"
                            layout="responsive">
                            <source src="$1"/>
                            <div fallback>
                                <p>This browser does not support the video element.</p>
                            </div>
                        </amp-video>';
        $this->loadScripts('amp-video');
        return $this->replace($pattern, $replacement, $content);
    }

    /**
     * Support <video> tags with multiple sources
     *
     * @param string  $content
     *
     * @return string Formatted content
     */
    public function parseMultiSourceVideo($content)
    {
        $pattern = '<video[^>]*?[^>]*?>(.*?)<\/video>';
        $sourcePattern = '<source[^>]*?src="(.*?)"[^>]*?type="(.*?)"[^>]*?>(<\/source>)?';
        $videoContent = $this->replace($pattern, "$1", $content);

        preg_match_all("/$sourcePattern/im", $videoContent, $matches);

        if (empty($matches[0])) {
            return $content;
        }

        $replacement = '<amp-video controls
                            width="640"
                            height="360"
                            layout="responsive">';

        foreach ($matches[0] as $index => $source) {
            $replacement .= '<source src="' . $matches[1][$index] . '" type="' . $matches[2][$index] . '"/>';
        }

        $replacement .=    '<div fallback>
                                <p>This browser does not support the video element.</p>
                            </div>
                        </amp-video>';

        $this->loadScripts('amp-video');
        return $this->replace($pattern, $replacement, $content);
    }

    /**
     * Object are not allowed by AMP.
     * Removes all the unsupported Object from the contend
     *
     * @param  string $content
     * @param  string $url
     * @return string Formatted content
     */
    public function removeUnsupportedObject($content, $url)
    {
        return $this->replace('<object[^>]*?>.*?<\/object>?', $this->getUnsupportedContent($url), $content);
    }

    /**
     * Interaction are not allowed by AMP.
     * Removes all the unsupported Interaction from the contend
     *
     * @param  string $content
     * @param  string $url
     * @return string Formatted content
     */
    public function removeUnsupportedInteraction($content, $url)
    {
        return $this->replace('<interaction[^>]*?>.*?<\/interaction>?', $this->getUnsupportedContent($url), $content);
    }

    /**
     * Img are not allowed by AMP.
     * Removes all the unsupported Img from the contend
     *
     * @param  string $content
     * @param  string $url
     * @return string Formatted content
     */
    public function removeUnsupportedImg($content, $url)
    {
        return $this->replace('<img[^>](\s|.)*?>', $this->getUnsupportedContent($url), $content);
    }

    /**
     * Iframes are not allowed by AMP.
     * Removes all the unsupported iframes from the contend
     *
     * @param  string $content
     * @param  string $url
     * @return string Formatted content
     */
    public function removeUnsupportedIframes($content, $url)
    {
        return $this->replace('<iframe[^>](\s|.)*?>(<\/iframe>)?', $this->getUnsupportedContent($url), $content);
    }

    /**
     * Parse Jwplayer content
     *
     * @param  string $content
     *
     * @return string Formatted content
     */
    public function parseJwplayerContent($content)
    {
        $pattern = '<script[^>]*?jwplatform\.com\/players\/(.*?)-(.*?)\.js[^>]*?><\/script>';
        $replacement = '<amp-jwplayer
                            data-player-id="$2"
                            data-media-id="$1"
                            layout="responsive"
                            width="500" height="360">
                        </amp-jwplayer>';
        $this->loadScripts('amp-jwplayer');
        return  $this->replace($pattern, $replacement, $content);
    }

    /**
     * Embeds are not allowed by AMP.
     * Removes all the unsupported embeds from the contend
     *
     * @param  string $content
     * @param  string $url
     * @return string Formatted content
     */
    public function removeUnsupportedEmbeds($content, $url)
    {
        return $this->replace('<embed[^>].*?>(<\/embed>)?', $this->getUnsupportedContent($url), $content);
    }

    /**
     * ApesterTags needs to be parsed to amp
     * @param string $content Post content
     *
     * @return string $content Post with amp-apester tags
     */
    public function parseApesterDivs($content)
    {
        $pattern = '<div(?:.*?)class="apester-media"(?:.*?)data-media-id="(.*?)"(?:.*?)height="(.*?)"(?:[^>]*?)><\/div>';
        $replace = '<amp-apester-media '.
                'height="$2" '.
                'data-apester-media-id="$1">'.
            '</amp-apester-media>';

        $this->findMatch($pattern, $content, function () {
            $this->loadScripts('amp-apester-media');
        });

        return $this->replace($pattern, $replace, $content);
    }

    /**
     * Support apester tags
     *
     * @param  String $content Html Content
     *
     * @return String $content Html Content Formatted
     */
    private function parseApesterTags($content)
    {
        $pattern = '<interaction.+id="(.+)">*?<\/interaction>';
        $replace = '<amp-apester-media '.
        'height="0" '.
        'data-apester-media-id="$1">'.
        '</amp-apester-media>';

        $this->findMatch($pattern, $content, function () {
            $this->loadScripts('amp-apester-media');
        });

        return $this->replace($pattern, $replace, $content);
    }

    /**
     * Inline styles are not supported, this function
     * removes them
     *
     * @param  string $content Post content
     *
     * @return string $content Post content formatted
     */
    private function removeStyleAttributes($content)
    {
        $pattern = '(<.*?)(style="(?:.*?)")(.*?>)';
        return $this->replace($pattern, '$1$3', $content);
    }

    /**
     * Remove unsupported link rel from the content
     *
     * @param  string $content Post content
     *
     * @return string $content Post content without the link rel
     */
    public function removeLinkStyleTags($content)
    {
        return $this->replace('<link(.+?)rel="stylesheet".*?>', '', $content);
    }

    /**
     * Remove unsupported style tag from the content
     *
     * @param string $content Post content
     *
     * @return string $content Post content without the style tag
     */
    public function removeStyleTags($content)
    {
        return $this->replace('<style(.*?)>(.|\s)*?<\/style>', '', $content);
    }

    /**
     * Parse "img" tags into a google-amp
     * compatible image tags
     *
     * @param  string $content Post content
     *
     * @return string $content Post content formatted
     */
    private function parseImages($content)
    {
        $patterns = [
            // src width height
            '<img((?:[^>])*?)src="([^">]*)"((?:[^>])*?)((width)="([\d]+)(?:px)?")((?:[^>])*?)((height)="([\d]+)(?:px)?")[^>]*?>'
            => '<amp-img src="$2" $4 $8 layout="responsive" class="image" style="max-width:$6px;"></amp-img>',
            // src height width
            '<img((?:[^>])*?)src="([^"]*)"((?:[^>])*?)((height)="([\d]+)(?:px)?")((?:[^>])*?)((width)="([\d]+)(?:px)?")[^>]*?>'
            => '<amp-img src="$2" $8 $4 layout="responsive" class="image" style="max-width:$10px;"></amp-img>',
            //  width height src
            '<img((?:[^>])*?)((width)="([\d]+)(?:px)?")((?:[^>])*?)((height)="([\d]+)(?:px)?")((?:[^>])*?)src="([^"]*)"((?:[^>])*?)[^>]*?>'
            => '<amp-img src="$10" $2 $6 layout="responsive" class="image" style="max-width:$4px;"></amp-img>',
            //  height width src
            '<img((?:[^>])*?)((height)="([\d]+)(?:px)?")((?:[^>])*?)((width)="([\d]+)(?:px)?")((?:[^>])*?)src="([^"]*)"((?:[^>])*?)[^>]*?>'
            => '<amp-img src="$10" $6 $2 layout="responsive" class="image" style="max-width:$8px;"></amp-img>',
            // width src height
            '<img((?:[^>])*?)((width)="([\d]+)(?:px)?")((?:[^>])*?)src="([^"]*)"((?:[^>])*?)((height)="([\d]+)(?:px)?")[^>]*?>' =>
            '<amp-img src="$6" $2 $8 layout="responsive" class="image" style="max-width:$4px;"></amp-img>',
            // height src width
            '<img((?:[^>])*?)((height)="([\d]+)(?:px)?")((?:[^>])*?)src="([^"]*)"((?:[^>])*?)((width)="([\d]+)(?:px)?")[^>]*?>' =>
            '<amp-img src="$6" $8 $2 layout="responsive" class="image" style="max-width:$10px;"></amp-img>',
            // no width and height ? maybe just height ? go default
            '<img((?:[^>])*?)src="([^"]*)"((?:[^>])*?)[^>]*?>' =>
            '<amp-img src="$2" width="auto" height="250" layout="fixed-height" class="image"></amp-img>'
        ];

        return $this->replaceEach($patterns, $content);
    }

    /**
     * Parse Twitter quotes into a google-amp
     * compatible embed
     *
     * @param  string $content Post content
     *
     * @return string $content Post content formatted
     */
    private function parseTwitterQuotes($content)
    {
        // remove the twitter script
        $scriptsPattern = '<script.*?platform\.twitter\.com\/widgets\.js.*?\/script>';
        $content =  $this->replace($scriptsPattern, '', $content);

        // check for nested tweets
        $nested_tweet = '(<blockquote.*?class="twitter-tweet".*)(?:<blockquote.*?class="twitter-tweet".*?\/blockquote>)';

        //  found ? remove the child
        $content = $this->replace($nested_tweet, '$1', $content);

        // find the rest of the tweets
        $pattern = '<(blockquote)([^>]*?class="twitter-.*?"[^>]*?>(\s|.)+?status\/([\d]+)(\s|.)+?<\/\1>)';
        $replacement = '<amp-twitter width=486 height=657
            layout="responsive"
            data-tweetid="$4">
            <blockquote placeholder
            $2
        </amp-twitter>';

        $this->findMatch($pattern, $content, function () {
            return $this->loadScripts('amp-twitter');
        });

        return $this->replace($pattern, $replacement, $content);
    }

    /**
     * Parse Instagram quotes into a google-amp
     * compatible embed
     *
     * @param  string $content Post content
     *
     * @return string $content Post content formatted
     */
    private function parseInstagramQuotes($content)
    {
        $pattern = '<blockquote(.*?)class=(\'|")instagram-media(\'|")(.*?|\s)*?instagram.com\/p\/(.*?)(\/|\?)(.*|\s)*?<\/blockquote>';
        $replace = '<amp-instagram
                data-shortcode="$5"
                width="320"
                height="322"
                data-captioned
                layout="responsive">
            </amp-instagram>';

        $this->findMatch($pattern, $content, function () {
            $this->loadScripts('amp-instagram');
        });

        $content = $this->replace($pattern, $replace, $content);

        // remove ig script
        $pattern = '(<(p)>)?<script[^>]*?instagram\.com(.*?)<\/script>(<\/\2>)?';
        $replace = '';
        return $this->replace($pattern, $replace, $content);
    }

    /**
     * Parse Vimeo embeds into a google-amp
     * compatible embed
     *
     * @param  string $content Post content
     *
     * @return string $content Post content formatted
     */
    private function parseVineEmbeds($content)
    {
        $pattern = '<(iframe)[^>]*?vine\.co\/v\/(.*?)\/(.|\s)*?width="([\d]+)"'.
            '(.|\s)*?height="([\d]+)"(.|\s)*?><\/\1>(\s|.)*?<(script)(.*?)<\/\9>';
        $replace = '<amp-vine width="$4" height="$6" data-vineid="$2" layout="responsive"></amp-vine>';

        $this->findMatch($pattern, $content, function () {
            return $this->loadScripts('amp-vine');
        });

        return $this->replace($pattern, $replace, $content);
    }

    /**
     * Parse ImgUr quotes into a google-amp
     * compatible embed
     *
     * @param  string $content Post content
     *
     * @return string $content Post content formatted
     */
    private function parseImgUrQuotes($content, $url)
    {
        $pattern = '<(blockquote) class="imgur-embed-pub[^>]*?data-id="(.*?)"(.*?)<\/\1>(\s?)';
        $replace = '<amp-iframe width=300 height=300 '.
        'sandbox="allow-scripts allow-same-origin allow-popups allow-popups-to-escape-sandbox" '.
        'layout="responsive" frameborder="0" '.
        'src="https://imgur.com/$2/embed?ref='.urlencode($url).'"></amp-iframe>';

        $this->findMatch($pattern, $content, function () {
            return $this->loadScripts('amp-iframe');
        });

        $content = $this->replace($pattern, $replace, $content);
        // remove the script
        $pattern = '<(script) src="\/\/s\.imgur.*?<\/\1>';
        return $this->replace($pattern, '', $content);
    }

    /**
     * Remove non-supported links attributes
     *
     * @param  string $content Post content
     *
     * @return string $content Post content formatted
     */
    private function parseVimeoEmbeds($content)
    {
        $pattern = '<(iframe)(.*?)vimeo\.com\/video\/([\d]+?)"(.*?)<\/\1>';
        $replace = '<amp-vimeo data-videoid="$3" layout="responsive" width="300" height="300"></amp-vimeo>';

        $this->findMatch($pattern, $content, function () {
            return $this->loadScripts('amp-vimeo');
        });

        return $this->replace($pattern, $replace, $content);
    }

    /**
     * Parse ooyala embeds into a google-amp
     * compatible embed
     *
     * @param  string $content Post content
     *
     * @return string $content Post content formatted
     */
    private function parseOoyalaEmbeds($content, $url)
    {
        $pattern = '<(script).*?src="http(s)?:\/\/player\.ooyala\.com\/iframe\.js(.*?)"><\/\1>';
        $replace = '<amp-iframe width=300 height=300 '.
                'sandbox="allow-scripts allow-same-origin allow-popups allow-popups-to-escape-sandbox" '.
                'layout="responsive" '.
                'frameborder="0" '.
                'src="https://player.ooyala.com/iframe.html$3&docUrl='.$url.'">'.
            '</amp-iframe>';

        $this->findMatch($pattern, $content, function () {
            $this->loadScripts('amp-iframe');
        });

        return $this->replace($pattern, $replace, $content);
    }

    /**
     * Wufoo embeds are not supported for amp
     * just replace the content with a message
     *
     * @param  string $content Post content
     *
     * @return string $content Post content formatted
     */
    private function parseWufooEmbeds($content, $url)
    {
        $iframePattern = '<iframe(.*?)src="((.*?)wufoo.com\/embed\/(.*?)?)"(.*?)<\/iframe>';
        $replace = '<amp-iframe width="500"
                height="281"
                layout="responsive"
                sandbox="allow-scripts allow-popups allow-same-origin"
                allowfullscreen
                frameborder="0"
                src="$2">
            </amp-iframe>';

        $content = $this->replace($iframePattern, $replace, $content);

        $this->findMatch($iframePattern, $replace, function () {
            return $this->loadScripts('amp-iframe');
        });

        // remove wufoo script
        $scriptPattern = '<script(.|\s)*?www\.wufoo\.com\/scripts(.|\s)*?<\/script>';
        $content = $this->replace($scriptPattern, '', $content);

        // remove wufoo html div
        $divPattern = '<div.*?id="wufoo-.*?>.*?<\/div>';
        return $this->replace(
            $divPattern,
            $this->getUnsupportedContent($url),
            $content
        );
    }

    /**
     * Parse Facebook Video Posts
     *
     * @param  string $content Post Content
     *
     * @return string $content Post Content Formatted
     */
    public function parseFacebookVideoPosts($content)
    {
        // Video posts
        // 23122016JD: Fb videos can come with different heights and widths. Let's regex them and set
        // the object based on their own dimensions rather than cropping them. ;)
        $videoPattern = '<iframe[^>]*?src="(.*?facebook\.com\/plugins\/video\.php[^"]href=(.*?)")(.*?)(width|height)="([^"].*?)"(.*?)(width|height)="([^"].*?)"[^>]*?>(.*?)<\/\iframe>';
        $videoReplacement = '<amp-facebook $4="$5" $7="$8"
                layout="responsive"
                data-embed-as="video"
                data-href="$2">
            </amp-facebook>';
        $this->findMatch($videoPattern, $content, function () {
            $this->loadScripts('amp-facebook');
        });

        $content = $this->replace($videoPattern, $videoReplacement, $content);

        // Makes sure that we have the right url encoding in the video embeds
        $videoUrlPattern = '(amp-facebook(?:.|\s)*?data-embed-as="video"(?:.|\s)*?data-href=")([^"&]+)(?:[^"]*?)(".*)';
        $videoMatches = preg_match_all('/' . $videoUrlPattern . '/', $content, $matches);
        if ($videoMatches) {
            // The preg_replace_callback function fails in some cases when the replacement $limit is not
            // specified.
            $limit = !empty($matches[0]) ? count($matches[0]) : 1;
            $content = preg_replace_callback('/' . $videoUrlPattern . '/', function ($match) use ($videoUrlPattern) {
                $video = $this->replace($videoUrlPattern, '$1$2$3', $match[0]);
                return urldecode($video);
            }, $content, $limit);
        }

        return $content;
    }

    /**
     * Parse Facebook Quotes
     *
     * @param  string $content Post Content
     *
     * @return string $content Post Content Formatted
     */
    private function parseFacebookQuotes($content)
    {
        //facebook quotes
        $quotesPattern = '<div class="fb-post".*?data-href="(.*?)"(.|\s)*?<blockquote(.|\s)*?<\/blockquote>(.|\s)*?<\/div>';
        $this->findMatch($quotesPattern, $content, function () {
            $this->loadScripts('amp-facebook');
        });
        $content = $this->replace(
            $quotesPattern,
            '<amp-facebook width=500 height=360
                layout="responsive"
                data-href="$1">
            </amp-facebook>',
            $content
        );

        return $content;
    }

    /**
     * Parse Facebook Video Quotes
     *
     * @param  string $content Post Content
     *
     * @return string $content Post Content Formatted
     */
    private function parseFacebookVideoQuotes($content)
    {
        $fbVideo = '<div class="fb-video"(.|\s)*?<blockquote(?:.*?)cite="(.*?)".*?>(.|\s)*?<\/blockquote>(.|\s)*?<\/div>';
        $this->findMatch($fbVideo, $content, function () {
            $this->loadScripts('amp-facebook');
        });
        $content = $this->replace(
            $fbVideo,
            '<amp-facebook width=auto height=360
                layout="fixed-height"
                data-embed-as="video"
                data-href="$2">
            </amp-facebook>',
            $content
        );

        return $content;
    }

    /**
     * Parse Facebook Video Embeds
     *
     * @param  string $content Post Content
     *
     * @return string $content Post Content Formatted
     */
    private function parseFacebookVideoEmbeds($content)
    {
        // Facebook video, just another video-format
        $facebookVideo = '<div.*?class="fb-post".*?data-href="((.*?)video\.php(.*?))".*?>(.|\s)*?fb-xfbml-parse-ignore(.|\s)*?<\/div>(.|\s)*?<\/div>';
        // remove facebook scripts
        $content = $this->replace('<script.*?(.|\s)*?facebook-jssdk(.|\s)*?\/script>', '', $content);
        $this->findMatch($facebookVideo, $content, function () {
            $this->loadScripts('amp-facebook');
        });
        $content = $this->replace(
            $facebookVideo,
            '<amp-facebook width=auto height=360
                layout="fixed-height"
                data-embed-as="video"
                data-href="$1">
            </amp-facebook>',
            $content
        );
        return $content;
    }

    /**
     * Parse Facebook Video Photo Embeds
     *
     * @param  string $content Post Content
     *
     * @return string $content Post Content Formatted
     */
    private function parseFacebookPhotoEmbeds($content)
    {
        // Facebook photo embeds
        $facebookVideo = '<div.*?class="fb-post".*?data-href="((.*?)photo\.php(.*?))".*?>(.|\s)*?fb-xfbml-parse-ignore(.|\s)*?<\/div>(.|\s)*?<\/div>';
        // remove facebook scripts
        $content = $this->replace('<script.*?(.|\s)*?facebook-jssdk(.|\s)*?\/script>', '', $content);
        $this->findMatch($facebookVideo, $content, function () {
            $this->loadScripts('amp-facebook');
        });
        $content = $this->replace(
            $facebookVideo,
            '<amp-facebook width=500 height=360
                layout="responsive"
                data-href="$1">
            </amp-facebook>',
            $content
        );
        return $content;
    }

    /**
     * Parse Facebook Regular (generic) Posts
     *
     * @param  string $content Post Content
     *
     * @return string $content Post Content Formatted
     */
    private function parseRegularFacebookPosts($content)
    {
        $patterns = [
            // facebook post link as url param 'href'
            '<iframe[^>]*?src="((.*?facebook\.com[^"]*?href=[^"]*?)([^"]*?facebook\.com[^"]*?)")[^>]*?>(.*?)<\/\iframe>' => '<amp-facebook width=486 height=657 layout="responsive" data-href="$3"></amp-facebook>',
            // full facebook link
            '<iframe[^>]*?src="(.*?facebook\.com[^"]*?")[^>]*?>(.*?)<\/\iframe>' => '<amp-facebook width=486 height=657 layout="responsive" data-href="$2"></amp-facebook>'
            ];

        // encode url
        $urls = $this->findMatch('(?:<iframe[^>]*?src=")(.*?facebook\.com[^"]*)', $content, function ($results) {
            return $results;
        });

        if (is_array($urls)) {
            foreach ($urls as $result) {
                if (!empty($result)) {
                    $content = str_replace($result, urldecode($result), $content);
                }
            }
        }

        $this->findMatch(array_keys($patterns)[1], $content, function () {
            $this->loadScripts('amp-facebook');
        });
        $content = $this->replaceEach($patterns, $content);

        return $content;
    }

    /**
     * Facebook embeds comes in different formats, the functions
     * bellow are ment to handle each format separately
     *
     * @param  string $content Post content
     *
     * @return string $content Post content (amp version)
     */
    private function parseFacebookEmbeds($content)
    {
        $content = $this->parseFacebookVideoPosts($content);
        $content = $this->parseFacebookVideoEmbeds($content);
        $content = $this->parseFacebookPhotoEmbeds($content);
        $content = $this->parseFacebookQuotes($content);
        $content = $this->parseFacebookVideoQuotes($content);
        $content = $this->parseRegularFacebookPosts($content);
        return $content;
    }

    /**
     * Finds an iframe with google maps and changes it to amp
     * @param  string $content Post content
     * @return  string $content (Amp version)
     */
    private function parseGoogleMapsIframes($content)
    {
        $pattern = '<iframe(.*?)src="((.*?)google.com\/maps(.*?)?)"(.*?)<\/iframe>';
        $replace = '<amp-iframe width="500"
                height="281"
                layout="responsive"
                sandbox="allow-scripts allow-popups allow-same-origin"
                allowfullscreen
                frameborder="0"
                src="$2">
            </amp-iframe>';

        $this->findMatch($pattern, $content, function () {
            return $this->loadScripts('amp-iframe');
        });

        $content = $this->replace($pattern, $replace, $content);

        return $content;
    }

    /**
     * Parse riddle-quiz embeds
     *
     * @param  string $content Post content
     *
     * @return string $content Post content formatted
     */
    private function parseAmpRiddleQuiz($content)
    {
        $pattern = '<div(?:.*?)data-rid-id=\"([\d]+)\"(?:.|\s)*?<\/div>(?:<p(?:.*?)class=\"link-riddle\"(?:.|\s)*?<\/p>)?';

        $this->findMatch($pattern, $content, function () {
            return $this->loadScripts('amp-riddle-quiz');
        });

        if (preg_match("/$pattern/im", $content, $matches)) {
            if (!empty($matches[0])) {
                $attributes = $this->getAttributes($matches[0]);
            }
        }

        $replace = '<amp-riddle-quiz layout="responsive" width="600" height="800" data-riddle-id="$1"></amp-riddle-quiz>';

        return $this->replace($pattern, $replace, $content);
    }

    /**
     * Parse Iframes into a google-amp
     * compatible iframes
     *
     * @param  string $content Post content
     *
     * @return string $content Post content formatted
     */
    private function parseIframes($content)
    {
        $pattern = '<(iframe)([^\>]*?)src=("|\')(?:http(?:s)?)?:?(?:\/\/)?([^"\']*)\3([^\>]*?)>(\s|.)*?<\/\1>';

        $this->findMatch($pattern, $content, function () {
            return $this->loadScripts('amp-iframe');
        });

        if (preg_match_all("/$pattern/im", $content, $matches)) {
            if (empty($matches[0])) {
                return $content;
            }
        }

        foreach ($matches[0] as $index => $match) {
            $attributes = $this->getAttributes($match);
            $height = "281";
            // If height is less 281 it could have some issues with aspect ratio and responsive layout
            if (!empty($attributes['height']) && is_numeric($attributes['height']) && intval($attributes['height']) > 281) {
                $height = $attributes['height'];
            }

            $pathInfo = parse_url($matches[4][$index]);
            $url = 'https://' . $pathInfo['path'];
            if (!empty($pathInfo['query'])) {
                $url .=  '?' . $pathInfo['query'];
            }
            $replace = '<amp-iframe width="500"
                    height="'.$height.'"
                    layout="responsive"
                    sandbox="allow-scripts allow-popups allow-same-origin"
                    allowfullscreen
                    frameborder="0"
                    src="'.$url.'">
                </amp-iframe>';

            $content = str_replace($match, $replace, $content);
        }
        return $content;
    }

    /**
     * Get the attributes from the given html tag
     *
     * @param string $string
     * @return null|array
     */
    protected function getAttributes($string)
    {
        preg_match_all("/(\S+)=[\"']?((?:.(?![\"']?\s+(?:\S+)=|[>\"']))+.)[\"']?/", $string, $data);
        if (!empty($data[1]) && !empty($data[2])) {
            $attributes = $data[1];
            $values = $data[2];
            return array_combine($attributes, $values);
        }
        return null;
    }

    /**
     * Object tags are not supported, this replaces them with
     * a placeholder
     *
     * @param  string $content Post content
     *
     * @return string $content Post content withot Object Embeds
     */
    public function removeObjectTags($content, $url)
    {
        return $this->replace(
            '<(object)(.|\s)*?<\/\1>',
            $this->getUnsupportedContent($url),
            $content
        );
    }

    /**
     * Map and area tags are not supported, this replaces them with
     * a placeholder
     *
     * @param  string $content Post content
     *
     * @return string $content Post content without Map Embeds
     */
    public function removeMapTags($content, $url)
    {
        return $this->replace(
            '<(map)(.|\s)*?<\/\1>',
            $this->getUnsupportedContent($url),
            $content
        );
    }

    /**
     * Remove some invalid attributes if present on the tags
     *
     *  @param string $content Post content
     *
     * @return  string $content Post content without the attributes
     */
    public function removeInvalidAttributes($content)
    {
        // Attribute xml
        $content = $this->replace("xml:|\s+clea(\S+)=[\"']?((?:.(?![\"']?\s+(?:\S+)=|[>\"']))+.)[\"']?", '', $content);

        // Remove type attribute if it's not an input tag
        $content = $this->replace("<(?!input|a)([^\>]*?) type=([\"']?((?:.(?![\"']?\s+(?:\S+)=|[>\"']))+.)[\"']?)>", '<$1>', $content);

        // Remove OnClick attribute
        $content = $this->replace('onclick="[^"]+"', '', $content);

        return $content;
    }

    /**
     * Remove script tags
     *
     * @param  string $content Post content
     *
     * @return string $content Post content without script tags
     */
    public function removeScriptTags($content)
    {
        // at this point, lets remove all the script tags to avoid amp errors
        return $this->replace('<script(.*?)>(.|\s)*?<\/script>', '', $content);
    }

    /**
     * Include form script if form is present
     * @param  string $content Post content
     *
     * @return string $content Post content with compatible forms
     */
    /**
     * Include form script if form is present
     * @param  string $content Post content
     *
     * @return string $content Post content with compatible forms
     */
    public function parseHtmlForms($content, $url)
    {
        // mailchimp forms are not working on amp
        $mailChimpPattern = '<form(.*?)action="(https?:?)?(\/\/)?(.*?)list-manage(.*?)".*?>(\s|.)*?<\/form>';

        if (preg_match('/'.$mailChimpPattern.'/im', $content)) {
            return $this->replace(
                $mailChimpPattern,
                $this->getUnsupportedContent($url),
                $content
            );
        }

        // Replace "action" attr by "action-xhr"
        $content = $this->replace(
            '(<form.*?)action="(https?:?)?(\/\/)?(.*)?"',
            '$1action-xhr="https://$4"',
            $content
        );

        // change "target" to _blank if exists
        $content = $this->replace(
            '(<form.*?target=").*?(".*?>)',
            '$1_blank$2',
            $content
        );

        // no target set ? add it
        if (!preg_match('<form.*?target=".*?>', $content)) {
            $content = $this->replace(
                '<form([^>]*?)(>)',
                '<form$1 target="_blank"$2',
                $content
            );
        }

        // don't forget about the script inclussion
        $this->findMatch('<form', $content, function () {
            $this->loadScripts('amp-form');
        });
        return $content;
    }

    /**
     * Strip pooldaddy scripts and returns a message
     * pooldaddy scripts are not supported yet
     *
     * @param  script $content Post content
     *
     * @return script $content Post content formatted
     */
    private function parsePooldaddyScripts($content, $url)
    {
        $pattern = '<(script).*?src="http(s)?:(:?\/\/)?static\.polldaddy.*?<\/\1>';
        $replacement = $this->getUnsupportedContent($url);
        return $this->replace($pattern, $replacement, $content);
    }

    /**
     * Some embeds are not supporte for google AMP yet
     * let's give them some default replacement
     *
     * @param  string $content Content string
     * @return string $content Formatted content string
     */
    private function getUnsupportedContent($url)
    {
        return '<strong>Some elements are not supported yet for this mobile version. </strong>'.
            '<p><a href="'.$url.'">You can see the full post content here.</a></p>';
    }

    /**
     * Parse PlayBuzz embeds into a google-amp
     * compatible embed
     *
     * @param  string $content Post content
     *
     * @return string $content Post content formatted
     */
    private function parsePlayBuzzEmbeds($content, $url)
    {
        $patterns = [
            // data-game after embed-by
            '<(div) class="pb_feed"[^>]*?data-embed-by="(.*?)"[^>]data-game="\/(.*?)\/(.*?)"(.*?)<\/\1>' =>
            '<amp-playbuzz width=300 height=300 '.
            'layout="responsive" '.
            'src="https://playbuzz.com/$3/$4?feed=true&src=http://playbuzz.com/$3/$4&game=$3/$4&social=true&parentHost='.$url.
            '&parentUrl='.$url.'"></amp-playbuzz>',


            // data-game before embed-by
            '<(div) class="pb_feed"[^>]*?data-game="\/(.*?)\/(.*?)"(.*?)data-embed-by="(.*?)"[^>]*?>(.*?)<\/\1>' =>
            '<amp-playbuzz width=300 height=300 '.
            'layout="responsive" '.
            'src="https://playbuzz.com/$2/$3?feed=true&src=http://playbuzz.com/$2/$3&game=$2/$3&social=true&parentHost='.$url.
            '&parentUrl='.$url.'"></amp-playbuzz>',

            // data-item after embed-by
            '<(div) class="pb_feed"[^>]*?data-embed-by="(.*?)"[^>]data-item="(.*?)\"(.*?)<\/\1>' =>
            '<amp-playbuzz width=300 height=300 '.
            'layout="responsive" '.
            'data-item="$3"></amp-playbuzz>',

            // data-item before embed-by
            '<(div) class="pb_feed"[^>]*?data-item="(.*?)"(.*?)data-embed-by="(.*?)"[^>]*?>(.*?)<\/\1>' =>
            '<amp-playbuzz width=300 height=300 '.
            'layout="responsive" '.
            'data-item="$2"></amp-playbuzz>',

            // data-id after class
            '<(div)[^>]*?class="playbuzz"[^>]*?data-id="(.*?)"[^>]*?>(.*?)<\/\1>' =>
            '<amp-playbuzz width=300 height=300 '.
            'layout="responsive" '.
            'data-item="$2"></amp-playbuzz>',

            // data-id before class
            '<(div)[^>]*?data-id="(.*?)"[^>]*?class="playbuzz"[^>]*?>(.*?)<\/\1>' =>
            '<amp-playbuzz width=300 height=300 '.
            'layout="responsive" '.
            'data-item="$2"></amp-playbuzz>'
        ];

        // Load scripts
        foreach ($patterns as $pattern => $replacement) {
            $this->findMatch($pattern, $content, function () {
                return $this->loadScripts('amp-playbuzz');
            });
            $content = $this->replace($pattern, $replacement, $content);
        }

        return $content;
    }

    /**
     * Remove non-supported links attributes
     *
     * @param  string $content Post content
     *
     * @return string $content Post content formatted
     */
    private function cleanLinks($content)
    {
        $patterns = [
            'target="_hplink"' => '',
            'alt=".*?"' => ''
        ];
        return $this->replaceEach($patterns, $content);
    }

    /**
     * Remove empty "<p>" tags
     *
     * @param  string $content Post content
     *
     * @return string $content Post content formatted
     */
    private function cleanEmptyLines($content)
    {
        $pattern = '(<p>(\&nbsp;)?<\/p>)';
        $replace = '';
        return $this->replace($pattern, $replace, $content);
    }

    /**
     * Get current post url
     *
     * @return string Post url
     */
    private function getPostUrl()
    {
        // a work around for testing (@TODO improve this)
        if (class_exists('Request')) {
            return str_replace('amp/', '', \Request::url());
        } else {
            return 'www.square1.io';
        }
    }

    /**
     * Removes images without or with empty src attr
     *
     * @param  String $content Html content
     *
     * @return String $content Formatted content
     */
    private function removeEmptyImageTags($content)
    {
        $pattern = '/<img(?:.|\s)*?>/';
        preg_match_all($pattern, $content, $matches);
        foreach ($matches[0] as $match) {
            if (preg_match('/<img(?:.|\s)*?src="[^"]+?"(?:.*?)>/', $match)) {
                continue;
            }
            $content = str_replace($match, '', $content);
        }
        return $content;
    }

    /**
     * Replace unsupported Embedly Card
     *
     * @param  string $content Article content
     *
     * @return string
     */
    private function parseEmbedlyCardQuote($content, $url)
    {
        $pattern = '<blockquote[^>]*?class="embedly-card(.*?)>(\s|.)*?<\/blockquote>';

        $replacement = $this->getUnsupportedContent($url);

        return $this->replace($pattern, $replacement, $content);
    }

    /**
     * Parse Brid Player
     *
     * @param  string $content Post content
     * @param  string $url     Post url
     *
     * @return string $content Post content formatted
     */
    private function parseBridPlayer($content, $url)
    {
        $pattern  = '\[(brid)(.*?)(video=("|\')([0-9]+)("|\'))(.*?)(player=("|\')([0-9]+)("|\'))(.*?)(title=("|\')(.*?)("|\'))\]';

        if (empty($_ENV['BRID_VIDEO_PARTNER_ID'])) {
            return $this->replace($pattern, $this->getUnsupportedContent($url), $content);
        }

        $replacement = '<amp-brid-player data-partner="'.$_ENV['BRID_VIDEO_PARTNER_ID'].'" data-player="$10" data-video="$5" layout="responsive" width="600" height="338"></amp-brid-player>';

        $this->findMatch($pattern, $content, function () {
            return $this->loadScripts('amp-brid-player');
        });

        return $this->replace($pattern, $replacement, $content);
    }

    /**
     * Remove param tags. This tag not allowed in AMP
     *
     * @param  string $content Post content
     *
     * @return string $content Post content formatted
     */
    public function removeParamTags($content)
    {
        $content = $this->replace('<(param)(.|\s)*?>', '', $content);

        return $content;
    }

    /**
     * Parse brightcove player
     *
     * @param  string $content Post content
     * @param  string $url Post content
     *
     * @return string $content Post content formatted
     */
    public function parseBrightcovePlayer($content, $url)
    {
        return $this->replace(
            '<(embed)((.|\s)*?)(src=("|\')((.|\s)*?)(brightcove\.com)((.|\s)*?)("|\'))((.|\s)*?)>',
            $this->getUnsupportedContent($url),
            $content
        );
    }

    /**
     * Remove embed tags from tubechop. This tag not allowed in AMP
     *
     * @param  string $content Post content
     * @param  string $url Post content
     *
     * @return string $content Post content without tubechop Embeds
     */
    public function removeEmbedTagsFromTubechop($content, $url)
    {
        return $this->replace(
            '<(embed)((.|\s)*?)(src=("|\')((.|\s)*?)(tubechop\.com)((.|\s)*?)("|\'))((.|\s)*?)>',
            $this->getUnsupportedContent($url),
            $content
        );
    }

    /**
     * Remove Javascript events. Not allowed in AMP
     *
     * @param  string $content Post content
     *
     * @return string $content Post content formatted
     */
    public function removeUnsupportedJsEvents($content)
    {
        // Remove Javascript events: onmouseout and onmouseover
        return $this->replace('(onmouseout=\"(.*?)\")|(onmouseover=\"(.*?)\")', '', $content);
    }

    /**
     * Parse image slider
     *
     * @param  string $content Post content
     *
     * @return string $content Post content formatted
     */
    private function parseImageSlider($content)
    {
        $pattern = '/<div(?:.*?)class="data-embed-slider(?:.*?)">(?:.*?|\n)(?:.*?)((<img((?:[^>])*?)src="([^"]*)"((?:[^>])*?)[^>]*?>){2})/';
        preg_match_all($pattern, $content, $matches);

        if (!empty($matches[1])) {
            foreach ($matches[1] as $images) {
                $ampImages = $this->replace('<img((?:[^>])*?)src="([^"]*)"((?:[^>])*?)[^>]*?>', '<amp-img src="$2" layout="fill" class="image"></amp-img>', $images);
                $replacement = '<amp-image-slider width="300" height="270" layout="responsive">'.$ampImages.'</amp-image-slider>';
                $content = str_replace($images, $replacement, $content);
            }
            $this->loadScripts('amp-image-slider');
        }

        return $content;
    }

    /**
     * Change value of target attribute from blank to _blank
     *
     * @param  string $content Post content
     *
     * @return string $content Post content formatted
     */
    public function changeValueTargetAttribute($content)
    {
        return $this->replace('target=("|\')blank("|\')', 'target="_blank"', $content);
    }

    /**
     * Parse Live Blog of Arena.im
     *
     * @param  string $content Post content
     *
     * @return string $content Post content formatted
     */
    public function parseLiveBlogArenaIm($content)
    {
        $pattern = '<div(.*?)id="arena-live"(.*?)data-publisher="(.*?)"(.*?)data-event="([0-9]+)"><\/div>';
        $replace = '<amp-iframe
                src="https://go.arena.im/embed?publisher=$3&event=$5"
                height="600"
                layout="fixed-height"
                frameborder="0"
                sandbox="allow-scripts allow-same-origin allow-popups allow-forms allow-modals">
                <amp-img src="https://arena.im/public/imgs/empty-photo-cover-event.png" layout="fixed-height" height="600" placeholder></amp-img>
            </amp-iframe>';

        $this->findMatch($pattern, $content, function () {
            return $this->loadScripts('amp-iframe');
        });

        $content = $this->replace($pattern, $replace, $content);

        return $content;
    }
}
