<?php

namespace Square1\Amp\Transformers\Interfaces;

/**
 * Transformable contract
 */
interface Transformable
{
    public function getRawContent($html);
    public function getFormattedContent($html);
    public function getScripts();
    public function getRawData();
}
