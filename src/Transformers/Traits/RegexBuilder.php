<?php

namespace Square1\Amp\Transformers\Traits;

/**
* Regex helpers class
*/
trait RegexBuilder
{

    /**
     * Decorates the preg_replace function
     *
     * @param  string $pattern     Regex pattern
     * @param  string $replacement Regex Replacement
     * @param  string $content     Content string
     *
     * @return string              Formatted string
     */
    public function replace($pattern, $replacement, $content)
    {
        $pattern = '/' . $pattern . '/im';

        if (preg_match($pattern, $content)) {
            $result = preg_replace($pattern, $replacement, $content);
        }

        // When something unexpected happens with the preg_replace function,
        // the result that we get is "null" or "empty", leaving an entire post
        // without content. What we need to do is return the original content in case
        // the preg_replace fails, to avoid errors in the front.
        if (empty($result)) {
            return $content;
        }

        return $result;
    }

    /**
     * Apply the preg_replace method to each pair of data in the
     * array [$pattern => $replacement] to the given content
     *
     * @param  array  $patterns collection of patterns and replacements
     * @param  string $content Content string
     *
     * @return string $content Formatted content
     */
    public function replaceEach($patterns = array(), $content = '')
    {
        foreach ($patterns as $pattern => $replacement) {
            $content = $this->replace($pattern, $replacement, $content);
        }

        return $content;
    }

    /**
     * Check for any match of a pattern in the given content
     *
     * @param  string  $pattern Regex pattern
     * @param  string  $content Content string
     * @param  mixed   $func    Callback function
     *
     * @return true|false|function callback
     */
    public function findMatch($pattern, $content, $func = true)
    {
        $pattern = '/'.$pattern.'/im';
        if (preg_match($pattern, $content, $results)) {
            return call_user_func($func, $results);
        }
        return false;
    }
}
