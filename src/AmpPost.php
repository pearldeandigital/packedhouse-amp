<?php

namespace Square1\Amp;

use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* Google Amp Post
* Access to the transformable class to get the
* Amp compatible post version
*/
class AmpPost
{

    private $transformer;
    public $content = '';
    public $formatted;
    public $scripts = '';
    public $original = null;

    /**
     * Call the passed transformable class and return
     * the response
     *
     * @param mixed         $data        Raw post data
     * @param Transformable $transformer Transformed data
     *
     */
    public function __construct($post)
    {
        $this->transformer = new PublisherPlusTransformer($post);
        $this->content($this->transformer->getRawContent());
        $this->original = $this->transformer->getRawData();
        return $this;
    }

    /**
     * Get the raw post content (article body)
     *
     * @return mixed
     */
    public function content($html = '')
    {
        if (!empty($html)) {
            $this->content = $html;
        }

        $this->content = $this->transformer->getRawContent($this->content);

        return $this;
    }

    /**
     * Get the amp-post content version (article body)
     *
     * @return mixed
     */
    public function getFormattedContent($html = '')
    {
        return $this->transformer->getFormattedContent($html);
    }

    /**
     * Get the required amp scripts to include in the
     * html scripts section
     *
     * @return string google-amp "<script>" tags
     */
    public function getScripts()
    {
        return $this->transformer->getScripts();
    }

    /**
     * Load amp scripts
     *
     * @param  String $script script-name ie: amp-iframe
     *
     * @return $this
     */
    public function loadScripts($script)
    {
        $this->transformer = $this->transformer->loadScripts($script);
        return $this;
    }
}
