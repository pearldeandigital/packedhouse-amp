<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class VimeoEmbedsTest extends TestCase
{

    public function testVimeoShouldUseAmpTag()
    {
        $post = $this->getPost($this->getVimeoEmbed());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseVimeoEmbeds', [$post['content']['formatted']]);

        $this->assertEquals($this->getVimeoFormatted(), $formatted);

        // twitter script should be loaded
        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-vimeo')), true);
    }

    private function getVimeoEmbed()
    {
        return '<iframe src="https://player.vimeo.com/video/190841648" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
    }

    private function getVimeoFormatted()
    {
        return '<amp-vimeo data-videoid="190841648" layout="responsive" width="300" height="300"></amp-vimeo>';
    }
}
