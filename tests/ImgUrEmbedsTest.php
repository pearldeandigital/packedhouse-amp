<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class ImgUrEmbedsTest extends TestCase
{

    public function testImgUrShouldUseAmpTag()
    {
        $post = $this->getPost($this->getImgUr());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseImgUrQuotes', [$post['content']['formatted'], 'www.square1.io']);

        $this->assertEquals($this->getImgUrFormatted(), $formatted);

        // twitter script should be loaded
        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-iframe')), true);
    }

    public function testImgUrScriptShouldBeRemoved()
    {
        $post = $this->getPost($this->getImgUrScirpt());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseImgUrQuotes', [$post['content']['formatted'], 'www.square1.io']);

        $this->assertEquals('<!-- some html comment -->', $formatted);
    }

    private function getImgUrScirpt()
    {
        return '<script src="//s.imgur.com/min/embed.js" async="" charset="utf-8"></script><!-- some html comment -->';
    }

    private function getImgUr()
    {
        return '<blockquote class="imgur-embed-pub" lang="en" data-id="gEC6EO4" '.
        'data-context="false"><p><a href="//imgur.com/gEC6EO4">Credit: The Gaming Bible</a></p></blockquote>';
    }

    private function getImgUrFormatted()
    {
        return '<amp-iframe width=300 height=300 '.
        'sandbox="allow-scripts allow-same-origin allow-popups allow-popups-to-escape-sandbox" '.
        'layout="responsive" frameborder="0" '.
        'src="https://imgur.com/gEC6EO4/embed?ref=www.square1.io"></amp-iframe>';
    }
}
