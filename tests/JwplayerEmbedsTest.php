<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class JwplayerEmbedsTest extends TestCase
{
    public function testJwplayerShouldUseAmpTag()
    {
        $post = $this->getPost($this->getJwplayerEmbed());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseJwplayerContent', [$post['content']['formatted']]);

        $this->assertEquals($this->getJwplayerFormatted(), $formatted);

        // twitter script should be loaded
        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-jwplayer')), true);
    }

    private function getJwplayerEmbed()
    {
        return '<script src="https://content.jwplatform.com/players/eRBP7e16-L01gJS2q.js"></script>';
    }

    private function getJwplayerFormatted()
    {
        return '<amp-jwplayer
                            data-player-id="L01gJS2q"
                            data-media-id="eRBP7e16"
                            layout="responsive"
                            width="500" height="360">
                        </amp-jwplayer>';
    }
}
