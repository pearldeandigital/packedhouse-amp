<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class AvoidUrlWithWordTypeTest extends TestCase
{

    public function testAvoidUrlWithWordType()
    {
        $post = $this->getPost($this->getUrlWithTypeClea());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'removeInvalidAttributes', [$post['content']['formatted'], 'square1.io']);

        $this->assertEquals($this->getUrlWithWordTypeWithoutRemoving(), $formatted);
    }

    private function getUrlWithWordTypeWithoutRemoving()
    {
        return '<a href="https://eu.topshop.com/webapp/wcs/stores/servlet/ProductDisplaysearchTermScope=3&amp;searchType=ALL&amp;viewAllFlag=false">link</a>';
    }

    private function getUrlWithTypeClea()
    {
        return '<a href="https://eu.topshop.com/webapp/wcs/stores/servlet/ProductDisplaysearchTermScope=3&amp;searchType=ALL&amp;viewAllFlag=false">link</a>';
    }
}