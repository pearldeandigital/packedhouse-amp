<?php

namespace Tests;

use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
 * AMP formatter test
 */
class RemoveUnsupportedJavascriptsEventsTest extends TestCase
{

    public function testRemoveUnsupportedJavascriptsEvents()
    {
        $post = $this->getPost($this->getContentWithJsEvents());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'removeUnsupportedJsEvents', [$post['content']['formatted']]);

        $this->assertEquals($this->getContentWithoutJsEvents(), $formatted);
    }

    private function getContentWithJsEvents()
    {
        return '<a target="_blank" onmouseover="window.status=\'http://www.strawberrynet.com\';return true;" onmouseout="window.status=\' \';return true;" href="http://www.anrdoezrs.net/click-2173270-10443020">mytest.com</a>';
    }

    private function getContentWithoutJsEvents()
    {
        return '<a target="_blank"   href="http://www.anrdoezrs.net/click-2173270-10443020">mytest.com</a>';
    }
}