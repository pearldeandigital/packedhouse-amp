<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class ParseInvalidTagsAttributesTest extends TestCase
{

    public function testInvalidTagsAttributesAreNotAllowed()
    {
        $post = $this->getPost($this->getInvalidAttributes());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'removeInvalidAttributes', [$post['content']['formatted'], 'square1.io']);

        $this->assertEquals($this->getInvalidAttributesRemoved(), $formatted);
    }

    /**
     * Test that words containing type are not being replaced
     */
    public function testTypeWithinAWord()
    {
        $content = '<div id="Brid_27761032" class="brid" style="width:16;height:9;" itemprop="video" itemscope itemtype="http://schema.org/VideoObject"> </div>';
        $post = $this->getPost($content);

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'removeInvalidAttributes', [$post['content']['formatted'], 'square1.io']);

        $this->assertEquals($formatted, $content);
    }

    private function getInvalidAttributesRemoved()
    {
        return '<p>The documentary was the idea of&nbsp;<span class="TextRun SCX165719458" lang="EN-IE"><span class="SpellingError SCX165719458">Bláthnaid</span></span><span class="TextRun SCX165719458" lang="EN-IE"> <span class="SpellingError SCX165719458">Treacy</span><span class="NormalTextRun SCX165719458">, who appeared in the show as </span></span><span class="TextRun SCX165719458" lang="EN-IE"><span class="NormalTextRun SCX165719458">Miley</span></span><span class="TextRun SCX165719458" lang="EN-IE"><span class="NormalTextRun SCX165719458"> and Biddy\'s daughter Denise - first cast at the age of three months (<em>beside Mary McEvoy in pic below</em>).</span></span></p>
        <p>When an otherworldly force wreaks havoc on a war-torn European city, an engineer teams up with an elite Special Ops unit to stop it.</p>
        <ul>
            <li>Launch Date: December 9</li>
        </ul>
        <p>
            <a  href="http://www.bingbongs.com/" target="_blank">http://www.bingbongs.com/</a>
        </p>
        ';
    }

    private function getInvalidAttributes()
    {
        return '<p>The documentary was the idea of&nbsp;<span class="TextRun SCX165719458" xml:lang="EN-IE"><span class="SpellingError SCX165719458">Bláthnaid</span></span><span class="TextRun SCX165719458" xml:lang="EN-IE"> <span class="SpellingError SCX165719458">Treacy</span><span class="NormalTextRun SCX165719458">, who appeared in the show as </span></span><span class="TextRun SCX165719458" xml:lang="EN-IE"><span class="NormalTextRun SCX165719458">Miley</span></span><span class="TextRun SCX165719458" xml:lang="EN-IE"><span class="NormalTextRun SCX165719458"> and Biddy\'s daughter Denise - first cast at the age of three months (<em>beside Mary McEvoy in pic below</em>).</span></span></p>
        <p>When an otherworldly force wreaks havoc on a war-torn European city, an engineer teams up with an elite Special Ops unit to stop it.</p>
        <ul type="disc">
            <li>Launch Date: December 9</li>
        </ul>
        <p>
            <a onclick="javascript: pageTracker._trackPageview(\'next-generation-groin-protection\');" href="http://www.bingbongs.com/" target="_blank">http://www.bingbongs.com/</a>
        </p>
        ';
    }
}
