<?php

namespace Tests;

use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class ParseRiddleQuizEmbedTest extends TestCase
{
    public function testRiddleQuizShouldUseAmpTag()
    {
        $post = $this->getPost($this->getRiddleQuizEmbed());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseAmpRiddleQuiz', [$post['content']['formatted'], 'www.square1.io']);

        $this->assertEquals($this->getRiddleQuizFormatted(), $formatted);

        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-riddle-quiz')), true);
    }

    private function getRiddleQuizEmbed()
    {
        return '<div class="riddle_target" data-rid-id="159580" data-auto-scroll="true">
                    <iframe title="embed-test" style="margin: 0 auto; maxWidth: 100%; width: 100%; height: 300px; border: 1px solid #cfcfcf;" src="https://www.riddle.com/showcase/159580/quiz"></iframe>
                </div>';
    }

    private function getRiddleQuizFormatted()
    {
        return '<amp-riddle-quiz layout="responsive" width="600" height="800" data-riddle-id="159580"></amp-riddle-quiz>';
    }
}
