<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
 * AMP formatter test
 */
class ParseLiveBlogArenaImTest extends TestCase
{
    public function testParseVideoEmbeds()
    {
        $post = $this->getPost($this->getLiveBlogArenaIm());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseLiveBlogArenaIm', [$post['content']['formatted']]);

        $this->assertEquals($this->getFormattedLiveBlogArenaIm(), $formatted);

        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-iframe')), true);
    }

    private function getLiveBlogArenaIm()
    {
        return '<div id="arena-live" data-publisher="balls" data-event="1543278412937"></div>';
    }

    private function getFormattedLiveBlogArenaIm()
    {
        return '<amp-iframe
                src="https://go.arena.im/embed?publisher=balls&event=1543278412937"
                height="600"
                layout="fixed-height"
                frameborder="0"
                sandbox="allow-scripts allow-same-origin allow-popups allow-forms allow-modals">
                <amp-img src="https://arena.im/public/imgs/empty-photo-cover-event.png" layout="fixed-height" height="600" placeholder></amp-img>
            </amp-iframe>';
    }
}
