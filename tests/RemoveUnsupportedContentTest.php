<?php

namespace Tests;

use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class RemoveUnsupportedContentTest extends TestCase
{
    public function testRemoveUsupportedEmbeds()
    {
        $post = $this->getPost($this->getContentWithParamTags());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'removeUnsupportedEmbeds', [$post['content']['formatted'], 'http//square1.io']);

        $this->assertEquals($this->getFormattedContent(), $formatted);
    }

    private function getContentWithParamTags()
    {
        return '<embed width="480" height="320" type="application/x-shockwave-flash" src="http://captiongenerator.com/Youtube_Captioner.swf" allowfullscreen="true" allowscriptaccess="always" flashvars="videoID=7902"></embed>';
    }

    private function getFormattedContent()
    {
        return '<strong>Some elements are not supported yet for this mobile version. </strong><p><a href="http//square1.io">You can see the full post content here.</a></p>';
    }

    public function testRemoveUnsuportedIframes()
    {
        $post = $this->getPost('<iframe id="twitter-widget-0" class="twitter-tweet twitter-tweet-rendered" title="Twitter Tweet" width="300" height="150" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen" data-tweet-id="654677491857002497"></iframe>');

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'removeUnsupportedIframes', [$post['content']['formatted'], 'http//square1.io']);

        $this->assertEquals($this->getFormattedContent(), $formatted);
    }

    public function testRemoveUnsuportedImg()
    {
        $post = $this->getPost('<img src=https://m0.her.ie/wp-content/uploads/cdn/default/0001/54/ed382bb8d642f330c3c2b33613db398822165026.jpeg width="625">');

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'removeUnsupportedImg', [$post['content']['formatted'], 'http//square1.io']);

        $this->assertEquals($this->getFormattedContent(), $formatted);
    }

    public function testRemoveUnsuportedInteraction()
    {
        $post = $this->getPost('<interaction>some data</interaction>');

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'removeUnsupportedInteraction', [$post['content']['formatted'], 'http//square1.io']);

        $this->assertEquals($this->getFormattedContent(), $formatted);
    }

    public function testRemoveUnsuportedObject()
    {
        $post = $this->getPost('<object>some data</object>');

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'removeUnsupportedObject', [$post['content']['formatted'], 'http//square1.io']);

        $this->assertEquals($this->getFormattedContent(), $formatted);
    }
}
