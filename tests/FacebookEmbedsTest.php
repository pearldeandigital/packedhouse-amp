<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class FacebookEmbedsTest extends TestCase
{

    public function testFacebookIframesShouldUseAmpIframe()
    {
        $post = $this->getPost($this->getFacebookVideoIframe());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseFacebookVideoPosts', [$post['content']['formatted']]);

        $this->assertEquals(trim($this->getFacebookVideoIframeFormatted()), trim($formatted));

        // facebook script should be loaded
        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-facebook')), true);
    }

    public function testFacebookPostsShouldUseAmpIframe()
    {
        $post = $this->getPost($this->getFacebookPost());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseFacebookQuotes', [$post['content']['formatted']]);

        $this->assertEquals(trim($this->getFacebookPostFormatted()), trim($formatted));

        // facebook script should be loaded
        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-facebook')), true);
    }

    public function testFacebookVideoQuotesShouldUseAmpTag()
    {
        $post = $this->getPost($this->getFacebookQuoteVideo());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseFacebookVideoQuotes', [$post['content']['formatted']]);

        $this->assertEquals(trim($this->getFacebookQuoteVideoFormatted()), trim($formatted));

        // facebook script should be loaded
        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-facebook')), true);
    }

    public function testFacebookVideoPostsShouldUseAmpTag()
    {
        $post = $this->getPost($this->getFacebookPostVideo());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseFacebookVideoEmbeds', [$post['content']['formatted']]);

        $this->assertEquals(trim($this->getFacebookPostVideoFormatted()), trim($formatted));

        // facebook script should be loaded
        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-facebook')), true);
    }

    public function testFacebookVideoPhotoShouldUseAmpTag()
    {
        $post = $this->getPost($this->getFacebookPostPhoto());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseFacebookPhotoEmbeds', [$post['content']['formatted']]);

        $this->assertEquals(trim($this->getFacebookPostPhotoFormatted()), trim($formatted));

        // facebook script should be loaded
        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-facebook')), true);
    }

    public function testFacebookRegularPostShouldUseAmpTag()
    {
        $post = $this->getPost($this->getFacebookRegularPost());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseRegularFacebookPosts', [$post['content']['formatted']]);

        $this->assertEquals(trim($this->getFacebookRegularPostFormatted()), trim($formatted));

        // facebook script should be loaded
        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-facebook')), true);
    }

    private function getFacebookVideoIframe()
    {
        return '<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fpaul.mcardle.9022%2Fvideos%2F10154674548920600%2F&amp;show_text=0&amp;width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allowfullscreen="true"></iframe>';
    }

    private function getFacebookVideoIframeFormatted()
    {
        return '<amp-facebook width="560" height="315"
                layout="responsive"
                data-embed-as="video"
                data-href="https://www.facebook.com/paul.mcardle.9022/videos/10154674548920600/">
            </amp-facebook>';
    }

    private function getFacebookPost()
    {
        return '<div class="fb-post" data-href="https://www.facebook.com/ciaran.kelly.568/videos/1390508187642870/" data-width="500" data-show-text="true">
<blockquote class="fb-xfbml-parse-ignore" cite="https://www.facebook.com/ciaran.kelly.568/videos/1390508187642870/">
<p>What a moment... Amazing scenes ??</p>
<p>Posted by <a href="https://www.facebook.com/ciaran.kelly.568">Ciarán Kelly</a> on <a href="https://www.facebook.com/ciaran.kelly.568/videos/1390508187642870/">Friday, 24 June 2016</a></p>
</blockquote>
</div>';
    }

    private function getFacebookPostFormatted()
    {
        return '<amp-facebook width=500 height=360
                layout="responsive"
                data-href="https://www.facebook.com/ciaran.kelly.568/videos/1390508187642870/">
            </amp-facebook>';
    }

    private function getFacebookQuoteVideo()
    {
        return '<div class="fb-video" data-href="https://www.facebook.com/828777003809568/videos/1187488944605037/" data-width="500" data-show-text="true">
<blockquote class="fb-xfbml-parse-ignore" cite="https://www.facebook.com/828777003809568/videos/1187488944605037/">
<p><a href="https://www.facebook.com/828777003809568/videos/1187488944605037/">Eric Cantona sings \'Will Grigg\'s On Fire\'</a>Eric Cantona belts out \'Will Grigg\'s On Fire\' (at 0:59) - and it\'s amazing!</p>
<p>Watch more Cantona \'Commissioner of Football\' videos: http://bit.ly/cantona-es</p>
<p>(Warning: strong language)</p>
<p>Posted by <a href="https://www.facebook.com/Eurosport-828777003809568/">Eurosport</a> on Friday, 24 June 2016</p>
</blockquote>
</div>';
    }

    private function getFacebookQuoteVideoFormatted()
    {
        return '<amp-facebook width=auto height=360
                layout="fixed-height"
                data-embed-as="video"
                data-href="https://www.facebook.com/828777003809568/videos/1187488944605037/">
            </amp-facebook>';
    }

    private function getFacebookPostVideo()
    {
        return '<div class="fb-post" data-href="https://www.facebook.com/video.php?v=914270315304441" data-width="500">
<div class="fb-xfbml-parse-ignore">
<blockquote cite="https://www.facebook.com/video.php?v=914270315304441">
<p>Never Satisfied. 61" at 293 lbs. #HuntGreatness</p>
<p>Posted by <a href="https://www.facebook.com/jjwatt1">JJ Watt</a> on <a href="https://www.facebook.com/video.php?v=914270315304441">Monday, April 13, 2015</a></p>
</blockquote>
</div>
</div>';
    }

    private function getFacebookPostVideoFormatted()
    {
        return '<amp-facebook width=auto height=360
                layout="fixed-height"
                data-embed-as="video"
                data-href="https://www.facebook.com/video.php?v=914270315304441">
            </amp-facebook>';
    }

    private function getFacebookPostPhoto()
    {
        return '<div class="fb-post" data-href="https://www.facebook.com/photo.php?fbid=1176043682436038&amp;set=a.419028134804267.91290435.100000912537853&amp;type=3" data-width="500">
<div class="fb-xfbml-parse-ignore">
<blockquote cite="https://www.facebook.com/photo.php?fbid=1176043682436038&amp;set=a.419028134804267.91290435.100000912537853&amp;type=3">
<p>Yep it\'s official I\'m NOT fighting in America anymore. That\'s boxing unfortunately. I\'ll be out in Manchester on...</p>
<p>Posted by <a href="https://www.facebook.com/jono.carroll.9">Jono Carroll</a> on <a href="https://www.facebook.com/photo.php?fbid=1176043682436038&amp;set=a.419028134804267.91290435.100000912537853&amp;type=3">Friday, 8 April 2016</a></p>
</blockquote>
</div>
</div>';
    }

    private function getFacebookPostPhotoFormatted()
    {
        return '<amp-facebook width=500 height=360
                layout="responsive"
                data-href="https://www.facebook.com/photo.php?fbid=1176043682436038&amp;set=a.419028134804267.91290435.100000912537853&amp;type=3">
            </amp-facebook>';
    }

    private function getFacebookRegularPost()
    {
        return '<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fpaul.mcardle.9022%2Fvideos%2F10154674548920600%2F&amp;show_text=0&amp;width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allowfullscreen="true"></iframe>';
    }

    private function getFacebookRegularPostFormatted()
    {
        return '<amp-facebook width=486 height=657 layout="responsive" data-href="https://www.facebook.com/paul.mcardle.9022/videos/10154674548920600/&amp;show_text=0&amp;width=560"></amp-facebook>';
    }

}
