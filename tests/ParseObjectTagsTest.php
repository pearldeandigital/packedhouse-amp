<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class ParseObjectTagsTest extends TestCase
{

    public function testObjectTagsAreNotAllowed()
    {
        $post = $this->getPost($this->getObject());

        $transformer = new PublisherPlusTransformer($post);

        $unsupportedContent = $this->invokeMethod($transformer, 'getUnsupportedContent', ['square1.io']);

        $formatted = $this->invokeMethod($transformer, 'removeObjectTags', [$post['content']['formatted'], 'square1.io']);

        $this->assertEquals($unsupportedContent, $formatted);
    }

    private function getObject()
    {
        return '<object width=486 height=657 src="square1.io"></object>';
    }
}
