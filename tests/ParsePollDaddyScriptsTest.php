<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class ParsePollDaddyScriptsTest extends TestCase
{

    public function testPollDaddyEmbedsAreNotSupported()
    {
        $post = $this->getPost($this->getScript());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parsePooldaddyScripts', [$post['content']['formatted'], 'square1.io']);

        $unsupportedContent = $this->invokeMethod($transformer, 'getUnsupportedContent', ['square1.io']);

        $this->assertEquals($unsupportedContent, $formatted);
    }

    private function getScript()
    {
        return '<script src="http://static.polldaddy.com/p/9440739.js" type="text/javascript" charset="utf-8"></script>';
    }
}
