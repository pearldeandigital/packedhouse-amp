<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class StylesFormattingTest extends TestCase
{

    public function testInlineStylesShouldBeRemoved()
    {
        $post = $this->getPost('<div style="width:400px; height:200px;">Hello World</div>');

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'removeStyleAttributes', [$post['content']['formatted']]);

        $this->assertEquals('<div >Hello World</div>', $formatted);
    }

    public function testLinkStyleTagsAreNotAllowed()
    {
        $post = $this->getPost('<link media="all" type="text/css" rel="stylesheet" href="http://www.joe.ie.dev/assets/css/joe/main.min-ac1de07c.css"><!-- some html comment -->');

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'removeLinkStyleTags', [$post['content']['formatted']]);

        $this->assertEquals('<!-- some html comment -->', $formatted);
    }

    public function testStyleTagsAreNotAllowed()
    {
        $post = $this->getPost('<style> .some-class { color: #000; padding: 15px; } </style><!-- some html comment -->');

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'removeStyleTags', [$post['content']['formatted']]);

        $this->assertEquals('<!-- some html comment -->', $formatted);
    }
}
