<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class ImagesFormattingTest extends TestCase
{
    public function testImagesShouldUseAmpTag()
    {
        $images = [
                // standard image
                '<img src="some-image-standard.jpg">' => '<amp-img src="some-image-standard.jpg" width="auto" height="250" layout="fixed-height" class="image"></amp-img>',
                // height after src
                '<img src="sh-image.jpg" height="150">' => '<amp-img src="sh-image.jpg" width="auto" height="250" layout="fixed-height" class="image"></amp-img>',
                // html with line break
                '<img src="some-image.jpg"
                height="150">' => '<amp-img src="some-image.jpg" width="auto" height="250" layout="fixed-height" class="image"></amp-img>',
                // width src height
                '<img width="254" src="wsh-image.jpg" height="190" />' => '<amp-img src="wsh-image.jpg" width="254" height="190" layout="responsive" class="image" style="max-width:254px;"></amp-img>',
                // src width height
                '<img src="swh-image.jpg" width="254" height="190" />' => '<amp-img src="swh-image.jpg" width="254" height="190" layout="responsive" class="image" style="max-width:254px;"></amp-img>',
                // src height width
                '<img src="shw-image.jpg" height="190" width="254"/>' => '<amp-img src="shw-image.jpg" width="254" height="190" layout="responsive" class="image" style="max-width:254px;"></amp-img>',
                //  width height src
                '<img width="254" height="190" src="whs-image.jpg"/>' => '<amp-img src="whs-image.jpg" width="254" height="190" layout="responsive" class="image" style="max-width:254px;"></amp-img>',
                // height width src
                '<img height="190" width="254" src="hws-image.jpg" />' => '<amp-img src="hws-image.jpg" width="254" height="190" layout="responsive" class="image" style="max-width:254px;"></amp-img>',
                // height source width
                '<img height="190" src="hsw-image.jpg" width="254" />' => '<amp-img src="hsw-image.jpg" width="254" height="190" layout="responsive" class="image" style="max-width:254px;"></amp-img>',
            ];

        foreach ($images as $unformatted => $expected) {
            $post = $this->getPost($unformatted);
            $transformer = new PublisherPlusTransformer($post);
            $formatted = $this->invokeMethod($transformer, 'parseImages', [$unformatted]);
            $this->assertEquals($expected, $formatted);
        };
    }

    public function testEmptyImagesShouldBeRemoved()
    {
        $images = [
            '<div>Some div</div><img class="some-class"><p>another tag</p>' => '<div>Some div</div><p>another tag</p>',
            '<img src="some-source.jpg"><img class="some-class">' => '<img src="some-source.jpg">',
            '<img src="">' => '',
            '<img src=""><img src="">' => '',
            '<img src="some-source.jpg"><img src="">' => '<img src="some-source.jpg">',
        ];
        foreach ($images as $source => $expected) {
            $post = $this->getPost($source);
            $transformer = new PublisherPlusTransformer($post);
            $formatted = $this->invokeMethod($transformer, 'removeEmptyImageTags', [$source]);
            $this->assertEquals($expected, $formatted);
        };
    }

    /**
     * This was an issue caused by this type of expressions
     * <img((?:\s|.)*?)src="([^"]*?)"((?:\s|.)*?)((height|width)="([\d]+)(?:px)?")((?:\s|.)*?)((height|width)="([\d]+?)(?:px)?")[^>]*?>
     * @return [type] [description]
     */
    public function testGreedyImageParseExpresions()
    {
        $source = '<h3><a href="image.jpg"><img class="alignnone  wp-image-57153" alt="some alt text" src="image.jpg" width="100%" height="300"></a></h3>'.
        '<h3>It’s been a bumpy ride for the band since 2005’s hit album, <i>A Certain Trigger </i>and the wonderfully infectious song, ‘Apply Some Pressure’, but things are looking up again....</h3>¡'.
        '<p><iframe src="//www.youtube.com/embed/-Ue193sAcrE" height="315" width="100%" allowfullscreen="" frameborder="0"></iframe></p>';
        $expected = '<h3><a href="image.jpg"><amp-img src="image.jpg" width="auto" height="250" layout="fixed-height" class="image"></amp-img></a></h3>'.
        '<h3>It’s been a bumpy ride for the band since 2005’s hit album, <i>A Certain Trigger </i>and the wonderfully infectious song, ‘Apply Some Pressure’, but things are looking up again....</h3>¡'.
        '<p><iframe src="//www.youtube.com/embed/-Ue193sAcrE" height="315" width="100%" allowfullscreen="" frameborder="0"></iframe></p>';
        $post = $this->getPost($source);
        $transformer = new PublisherPlusTransformer($post);
        $formatted = $this->invokeMethod($transformer, 'parseImages', [$source]);
        $this->assertEquals($expected, $formatted);
    }

    /**
     * When the content have multiple patterns that matches with the expression
     * the portion of '(\s|.)*?' was taking all the content in the middle and
     * making the preg_replace returning null as result
     */
    public function testImageFormattingWithMultipleMatchesInTheSameContent()
    {
        $source = '<img class="alignnone  wp-image-57153" alt="some alt text" src="image.jpg">'.
        '<h3>It’s been a bumpy ride for the band since 2005’s...</h3>¡'.
        '<img class="alignnone  wp-image-57153" alt="some alt text" src="image.jpg" width="100px" height="300">';

        $post = $this->getPost($source);
        $transformer = new PublisherPlusTransformer($post);
        $formatted = $this->invokeMethod($transformer, 'parseImages', [$source]);
        $this->assertNotEmpty($formatted);
    }

    public function testImageFormattingWithMultipleExactMatches()
    {
        $source = '<img class="alignnone  wp-image-57153" alt="some alt text" src="image.jpg" width="100px" height="300">'.
        '<h3>It’s been a bumpy ride for the band since 2005’s...</h3>¡'.
        '<img class="alignnone  wp-image-57153" alt="some alt text" src="image.jpg" width="100px" height="300">';

        $post = $this->getPost($source);
        $transformer = new PublisherPlusTransformer($post);
        $formatted = $this->invokeMethod($transformer, 'parseImages', [$source]);
        $this->assertNotEmpty($formatted);
    }
}
