<?php

namespace Tests;

use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class BridPlayerTest extends TestCase
{

    public function testBridPlayerShouldUseAmpTag()
    {
        $post = $this->getPost($this->getBridPlayerEmbed());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseBridPlayer', [$post['content']['formatted'], 'www.square1.io']);

        $this->assertEquals($this->getBridPlayerFormatted(), $formatted);

        // Brid Player script should be loaded
        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-brid-player')), true);
    }

    private function getBridPlayerEmbed()
    {
        return '[brid video="167669" player="8210" title="Media Analysis NETFLIXHow NARCOS got it wrong"]';
    }

    private function getBridPlayerFormatted()
    {
        return '<amp-brid-player data-partner="264" data-player="8210" data-video="167669" layout="responsive" width="600" height="338"></amp-brid-player>';
    }
}
