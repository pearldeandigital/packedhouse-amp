<?php

namespace Tests;

use Square1\Amp\AmpPost;
use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class ParseApesterTagsTest extends TestCase
{

    public function testApesterTagsShouldUseAmpFormat()
    {
        $content = '<p><interaction id="55238a827fdf079b4e297d82"></interaction></p>';

        $post = $this->getPost($content);

        $transformer = new PublisherPlusTransformer($post);

        $expected = '<p><amp-apester-media '.
            'height="0" '.
            'data-apester-media-id="55238a827fdf079b4e297d82">'.
            '</amp-apester-media></p>';

        $formatted = $this->invokeMethod($transformer, 'parseApesterTags', [$content]);

        $this->assertEquals($expected, $formatted);

        // apester script should be loaded
        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-apester-media')), true);
    }

    public function testApesterDivsShouldUseAmpFormat()
    {
        $content = '<p><div class="apester-media" data-media-id="59a693521868a8c438dfd37b" height="350"></div></p>';

        $post = $this->getPost($content);

        $transformer = new PublisherPlusTransformer($post);

        $expected = '<p><amp-apester-media '.
            'height="350" '.
            'data-apester-media-id="59a693521868a8c438dfd37b">'.
            '</amp-apester-media></p>';

        $formatted = $this->invokeMethod($transformer, 'parseApesterDivs', [$content]);

        $this->assertEquals($expected, $formatted);

        // apester script should be loaded
        $this->assertTrue(is_int(strpos($transformer->getScripts(), 'amp-apester-media')), true);
    }
}
