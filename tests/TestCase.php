<?php

namespace Tests;

use Square1\Amp\AmpPost;

/**
* Base test class
*/
class TestCase extends \PHPUnit_Framework_TestCase
{
    public function getAmpInstance($post)
    {
        return new AmpPost($post);
    }

    public function getPost($html = '')
    {
        $default = '<div>html content</div>';

        $html = ! empty($html) ? $html : $default;

        $post['content']['formatted'] = $html;

        return $post;
    }

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object    Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
