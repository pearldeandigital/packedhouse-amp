<?php

namespace Tests;

use Square1\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class RemoveParamTagsTest extends TestCase
{

    public function testRemoveParamTags()
    {
        $post = $this->getPost($this->getContentWithParamTags());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'removeParamTags', [$post['content']['formatted']]);

        $this->assertEquals($this->getContentWithoutParamTags(), $formatted);
    }

    private function getContentWithParamTags()
    {
        return '<div class="content"><param name="src" value="http://swf.tubechop.com/tubechop.swf?vurl=unEmG6AnlaY&amp;start=43&amp;end=80&amp;cid=1577334" /><param name="allowfullscreen" value="true" />My content</div>';
    }

    private function getContentWithoutParamTags()
    {
        return '<div class="content">My content</div>';
    }
}